const express = require('express');
const Route = express.Router();

const dashboardController = require('./dashboardController');
const authMiddleware = require('../../middleware/auth');

Route.get('/:id', authMiddleware.authentication, dashboardController.getDataDashboard);

module.exports = Route;
