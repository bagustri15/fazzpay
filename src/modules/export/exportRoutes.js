const express = require('express');
const Route = express.Router();
const authMiddleware = require('../../middleware/auth');

const exportController = require('./exportController');

Route.get('/transaction/:id', authMiddleware.authentication, exportController.exportTransaction);

module.exports = Route;
