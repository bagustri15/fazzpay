const redis = require('../config/redis');
const helper = require('../helpers/wrapper');
const jwt = require('jsonwebtoken');

module.exports = {
  authentication: async (req, res, next) => {
    try {
      let token = req.headers.authorization;
      if (!token) {
        return helper.response(res, 403, 'Please login first !');
      }
      token = token.split(' ')[1];

      jwt.verify(token, 'RAHASIA', async (error, resultToken) => {
        if (error) {
          return helper.response(res, 403, error.message);
        } else {
          const result = await redis.get(`accessToken:${token}`);
          if (result != null) {
            req.decodeToken = resultToken;
            next();
          } else {
            return helper.response(res, 403, 'Your token is destroyed please login again !');
          }
        }
      });
    } catch (error) {
      return helper.response(res, 403, error.message);
    }
  },
};
