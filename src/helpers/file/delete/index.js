const cloudinary = require('../../../config/cloudinary');

const deleteFile = (fileName) =>
  new Promise((resolve, reject) => {
    const file = fileName.split('.')[0];
    cloudinary.uploader.destroy(file, function (result) {
      resolve(result);
    });
  });

module.exports = deleteFile;
