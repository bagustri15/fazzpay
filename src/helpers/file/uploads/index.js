const multer = require('multer');
const { CloudinaryStorage } = require('multer-storage-cloudinary');
const helper = require('../../../helpers/wrapper');
const cloudinary = require('../../../config/cloudinary');

const storage = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: 'Fazzpay',
  },
});

const limits = 1024 * 1024 * 0.5;

const fileFilter = (req, file, cb) => {
  // Accept images only
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
    req.fileValidationError = 'Only image files are allowed!';
    return cb(new Error('Only image files are allowed!'), false);
  }
  cb(null, true);
};

const upload = multer({
  storage,
  limits: { fileSize: limits },
  fileFilter,
}).single('image');

const uploadFilter = (req, res, next) => {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      return helper.response(res, 401, err.message, null);
    } else if (err) {
      // An unknown error occurred when uploading.
      return helper.response(res, 401, err.message, null);
    }
    next();
    // Everything went fine.
  });
};

module.exports = uploadFilter;
