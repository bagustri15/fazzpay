const nodemailer = require('nodemailer');
const mustache = require('mustache');
const fs = require('fs');
const path = require('path');
const clientId = process.env.MAIL_CLIENT_ID;
const clientSecret = process.env.MAIL_CLIENT_SECRET;
const refreshToken = process.env.MAIL_REFRESH_TOKEN;
const { google } = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const OAuth2_client = new OAuth2(clientId, clientSecret);
OAuth2_client.setCredentials({
  refresh_token: refreshToken,
});

const sendMail = (data) =>
  new Promise((resolve, reject) => {
    const { to, subject, template } = data;
    const filePath = path.join(__dirname, `../../templates/email/${template}`);
    const fileTemplate = fs.readFileSync(filePath, 'utf8');
    const accessToken = OAuth2_client.getAccessToken;

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: 'memo.in.aja@gmail.com',
        clientId: clientId,
        clientSecret: clientSecret,
        refreshToken: refreshToken,
        accessToken: accessToken,
      },
    });

    const mailOptions = {
      from: '"FazzPay 👻" <fazzpay@gmail.com>', // sender address
      to: to, // list of receivers
      subject: subject, // Subject line
      html: mustache.render(fileTemplate, { ...data }),
    };
    transporter.sendMail(mailOptions, async (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info.response);
      }
    });
  });

module.exports = sendMail;
