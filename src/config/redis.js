require('dotenv').config();
const { createClient } = require('redis');

const client = createClient({
  socket: {
    host: process.env.RDS_HOST,
    port: process.env.RDS_PORT,
  },
  password: process.env.RDS_PASS,
});

(async () => {
  client.connect();
  client.on('connect', () => {
    console.log("You're now connected db redis !");
  });
})();

module.exports = client;
