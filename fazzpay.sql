-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2022 at 02:11 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fazzpay`
--

-- --------------------------------------------------------

--
-- Table structure for table `topup`
--

CREATE TABLE `topup` (
  `id` varchar(255) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topup`
--

INSERT INTO `topup` (`id`, `userId`, `amount`, `status`, `createdAt`, `updatedAt`) VALUES
('a7d4596a-56fb-434c-b239-0d7c853c39e9', 'e8e58876-8c48-44c4-92da-5d19f87bc074', 5000, 'success', '2022-05-20 10:50:07', '2022-05-20 10:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `id` varchar(255) NOT NULL,
  `senderId` varchar(255) NOT NULL,
  `receiverId` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `notes` text NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer`
--

INSERT INTO `transfer` (`id`, `senderId`, `receiverId`, `amount`, `balance`, `notes`, `status`, `createdAt`, `updatedAt`) VALUES
('e0d56c6f-1da7-4eca-b5a1-fb77637134bf', 'e8e58876-8c48-44c4-92da-5d19f87bc074', 'fce5d0fa-4ba0-468c-98fa-d757bef2e89b', 5000, 100000, 'Pembayaran Pajak', 'success', '2022-05-20 10:56:27', '2022-05-20 10:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) DEFAULT '',
  `noTelp` varchar(20) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `balance` int(11) DEFAULT 0,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `keysChangePassword` varchar(6) DEFAULT NULL,
  `keysVerifyAccount` varchar(6) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `noTelp`, `image`, `balance`, `email`, `password`, `pin`, `status`, `keysChangePassword`, `keysVerifyAccount`, `createdAt`, `updatedAt`) VALUES
('e8e58876-8c48-44c4-92da-5d19f87bc074', 'User', '2', '0219876543', NULL, 100000, 'user2@gmail.com', '$2b$10$3fvQT7bURHcbiavHVRxiFu71oHHRIYl5ATn8T11aho6x002NyeirS', '$2b$10$ZMIXvunDzqfE1hW8M3P9deyr8oVbPpcBNuXjydVL4v43mXEgUGSMO', 1, NULL, NULL, '2022-05-20 09:52:36', '2022-05-22 11:28:07'),
('fce5d0fa-4ba0-468c-98fa-d757bef2e89b', 'User', '1', '0213456789', NULL, 1005000, 'user1@gmail.com', '$2b$10$U260zew2NKyK4eN5mYAv0esJyA5cEb.ABc9SKB3hudLr4Lcdc70c2', '$2b$10$TfuZUR1lmpadcUiLb7bWOOGwlkt6NE6lsjLg8Uv8Y86KV1o0t3M0m', 1, NULL, NULL, '2022-05-20 09:50:12', '2022-05-20 10:56:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `topup`
--
ALTER TABLE `topup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
